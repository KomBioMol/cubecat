import numpy as np
import os
import sys
import tarfile


class Cube:
    def __init__(self, fname, frame=1):
        self.file = fname
        self.frame = frame
        self.atoms = []
        fh = open(self.file)
        [fh.readline() for _ in range(2)]
        natoms = int(fh.readline().split()[0])
        self.box = []
        for i in range(3):
            self.box.append(float(fh.readline().split()[i + 1]))
        for i in range(natoms):
            line = fh.readline().split()
            self.atoms.append(Atom(line[0], line[2:], i + 1))
        fh.close()
    
    def __repr__(self):
        return "File {} with {} atoms".format(self.file, len(self.atoms))
    
    def as_pdb(self):
        content = ["CRYST1 {:8.3f} {:8.3f} {:8.3f}  90.00  90.00  90.00 P 1           1".format(*self.box)]
        content.append("MODEL     {:>4d}".format(self.frame))
        for a in self.atoms:
            content.append(a.as_pdb())
        content.append("ENDMDL")
        return '\n'.join(content)
    
    def save_pdb(self, filename):
        with open(filename, 'w') as outfile:
            outfile.write(self.as_pdb())


class Atom:
    elements = {1: "H", 3: "Li", 5: "B", 6: "C", 7: "N", 8: "O", 9: "F", 15: "P", 11: "Na", 12: "Mg", 16: "S", 17: "Cl",
                19: "K", 20: "Ca", 35: "Br"}
    
    def __init__(self, atnum, coords, index):
        self.atnum = int(atnum)  # TODO allow to specify residue ranges
        self.coords = np.array([float(x) for x in coords]) * 0.53
        self.index = int(index)
    
    def as_pdb(self):
        return "ATOM   {:>4d} {:>4s}  QM     1    {:8.3f}{:8.3f}{:8.3f}  1.00  0.00".format(self.index, Atom.elements[
            self.atnum] + str(self.index), *self.coords)
    
    def __repr__(self):
        return "atom {}{}".format(Atom.elements[self.atnum], str(self.index))


class CubeTraj:
    def __init__(self, prefix):
        cubelist = sorted([f for f in os.listdir('.') if f.startswith(prefix)],
                          key=lambda x: int(x.split('_')[-1].split('.')[0]))
        self.cubes = [Cube(name, n) for n, name in enumerate(cubelist)]
    
    def write_traj(self, fname):
        with open(fname, 'a') as trajfile:
            for n, cube in enumerate(self.cubes):
                trajfile.write(cube.as_pdb())
                if n % 10 == 0:
                    print("Processing file {}...".format(cube.file))
    
    def __repr__(self):
        return "A Cubetraj object containing {} cube files".format(len(self.cubes))


if __name__ == "__main__":
    c = CubeTraj(sys.argv[1])
    c.write_traj(sys.argv[2])
    try:
        tarf = sys.argv[3]
        print(tarf)
    except IndexError:
        pass
    else:
        counter = 1
        if not tarf.endswith('tgz') and not tarf.endswith('tar.gz'):
            tarf = tarf + '.tgz'
        while tarf in os.listdir('.'):
            tarf = f'part{counter}_' + tarf.replace(f'part{counter-1}_', '')
            counter += 1
        with tarfile.open(tarf, "w:gz") as tar_handle:
            for n, cube in enumerate(c.cubes):
                tar_handle.add(cube.file)
                if n % 10 == 0:
                    print("Compressing file {}...".format(cube.file))
